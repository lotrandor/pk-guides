---
title: "Antergos Rescue"
date: 2018-02-22T10:53:37+01:00
description: "Jak zachranit instalaci Antergos Linuxu"
featured_image: '/images/hero.png'
tags: ["archlinux"]
draft: true
---

Nedávno jsem musel řešit obnovu startu instalace Antergos Linux poté, co se následkem přerušeného updatu jádra podařilo systém dostat do stavu, kdy bootoval jen do nouzového shellu. Postup jsem sepsal níže, třeba se to někomu hodí.
<!--more--> 

## start from live

### mount root and boot
```
mkdir /mnt/arch
sudo mount /dev/mapper/AntergosVG-AntergosRoot /mnt/arch
sudo mount /dev/sda1 /mnt/arch/boot

ls /mnt/arch/boot

grub initframs ....
```

### chroot the newly mounted filesystem

```
cd /mnt/arch
mount -t proc proc proc/
mount -t sysfs sys sys/
mount -o bind /dev dev/
chroot . /bin/bash
```

### start ethernet adapter

```
ifconfig

enp38s0 shws up

dhcpcd enp38s0
```

### refresh all packages and upgrade
```
sudo pacman -Syy

sudo pacman -Syu
```
### Reinstall udev and mkinitcpio
```
sudo pacman -S udev
sudo pacman -S mkinitcpio
```
### Create the initial ramdisk environment
```
sudo mkinitcpio -p linux

exit
reboot
```