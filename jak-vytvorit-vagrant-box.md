---
title: "Jak Vytvorit Vagrand Box"
date: 2018-02-22T11:54:32+01:00
featured_image: '/images/hero.png'
tags: ["vagrant"]
draft: true
---

Pro vytvoření nového boxu můžete mít řadu důvodů. Ať už potřebujete 
přidat pár úprav k existujícímu boxu, nebo nějakou prozatím neexistující 
kombinaci runtimů. Dalším důvodem může být potřeba vytvoření clusteru 
virtuálních strojů ... což je i náš případ.

<!--more-->

# Jak vytvořit Vagrant Box od začátku

Článek je inspirovaný prací Tylera Birda z [engineyard.com.org](https://www.engineyard.com/blog/building-a-vagrant-box)

## Proč vytvářet další vagrant box?

Pro vytvoření nového boxu můžete mít řadu důvodů. Ať už potřebujete 
přidat pár úprav k existujícímu boxu, nebo nějakou prozatím neexistující 
kombinaci runtimů. Dalším důvodem může být potřeba vytvoření clusteru 
virtuálních strojů ... což je i můj případ.

## Přípravy

V dalších krocích budeme potřebovat:

* [vagrant](https://www.vagrantup.com/downloads.html)
* [virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Debian - netinst amd64](https://www.debian.org/distrib/), [přímý 
link](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.2.1-amd64-netinst.iso)

## Vytvoření boxu

Jako provider virtualiuace použijeme VirtualBox z důvodu otevřené 
licence. Uživatelský účet pojmenujeme **vagrant** s heslem **vagrant**. 
Případná varovná hlášení o malé síle hesla budeme pro tentokrát 
ignorovat.

Ve VirtualBoxu založíme nový virtuální stroj s těmito parametry

* Jméno: vagrant-stretch64
* Typ: Linux
* Verze: Debian (64-bit)
* Velikost paměti: 2048 MB
* Vytvořit nový virtuální pevný disk: VMDK, 8GB, Dynamicky alokované

V nastavení virtuálního stroje provedeme tyto změny v hardwaru

* Úložiště: řadič IDE - obraz instalačního CD/DVD
* Audio: vypnout
* USB: vypnout
* Síť: NAT
* Předávání portů: Pokročilé volby sítě; Název: SSH, Protokol: TCP, IP 
adresa hostitele: prázdné, Port hostitele: 222, IP adresa hosta: 
prázdné, Port hosta: 22

## Instalace operačního systému

* provedeme rychlou instalaci operačního systému v textovém režimu
* název boxu: debian
* IP adresa: detekována z interního DHCP VirtualBoxu
* Automatické rozdělení disku
* heslo roota: vargant
* jméno uživatele: vagrant
* heslo uživatele: vagrant
* instalované balíky: bez desktop prostředí, bez print serveru, přidat 
ssh server

## Instalace SUDO a nastavení sudo bez hesla

Zalogujeme se do systému

```
Debian GNU/Linux 9 debian tty1

debian login: root
Password:

root@debina:~# _
```

Aktualizujeme repozitáře & upgradujeme systém
```
apt-get update -y && apt-get upgrade -y
```

V případě, že je v cestě k repozitářům CD/DVD, můžeme jej odstranit 
zakomentováním příslušného řádku v souboru /etc/apt/sources.list. Na 
stejném místě je možné doplnit cestu k dalším zrcadlům s repozitáři.

Nakonec rebootujeme systém pro případ aktualizace kernelu

```
shutdown -r now
```

## Instalace Vagrant klíče

Pro komunikaci přes SSH z hostitelského systému potřebujeme, aby měl SSH 
server hostovaného stroje nainstalovaný **nezabezpečený vagrant klíč**. 
Tento klíč může mít kdokoliv, kdo si je jej schopen stáhnout z 
internetu, odtud nezabezpečený. 

Přihlásíme se opětovně ke virtuálínmu stroji, tentokrál s uživatelem 
vagrant
Stáhneme a naistalujeme klíč

```
mkdir -p /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
wget --no-check-certificate \
    https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub \
    -O /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh    
```

## Instalace a configurace OpenSSH Serveru

Pokud jste neinstalovali SSH server na počátku instalace, můžete to nyní 
napravit

## Instalace Přídavků pro hosta

## Zabalení boxu

Před zabalením boxu potřebujeme odstranit případnou fragmentaci disků 
pro lepší kompresi.

```
sudo dd if=/dev/zero of=EMPTY bs=1M
sudo rm -f /EMPTY
```

Přesuneme se do adresáře, kam chceme umístit hotový vagrant box a 
spustíme následující příkaz, který provede zabalení virtuálního stroje s 
odpovídajícím názvem

```
vagrant package --base vagrant-stretch64
==> vagrant-stretch64: Clearing any previously set forwarded ports...
==> vagrant-stretch64: Exporting VM...
==> vagrant-stretch64: Compressing package to: 
C:/develHome/boxes/package.box
```

Výsledkem operace je soubor **package.box** v aktuálním adresáři.

## Otestování boxu

Z aktulního adresáře je možné box otestovat. Pokud jste něco pokazili 
cestou sem, projeví se to nyní.

```
vagrant box add stretch64 package.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'stretch64' (v0) for provider:
    box: Unpacking necessary files from: 
file://C:/develHome/boxes/package.box
    box:
==> box: Successfully added box 'stretch64' (v0) for 'virtualbox'!

vagrant init stretch64
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'stretch64'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: 
boxes_default_1511276028482_1120
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
     default: Vagrant insecure key detected. Vagrant will automatically 
replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH 
key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this 
VM! Guest
    default: additions are required for forwarded ports, shared folders, 
host only
    default: networking, and more. If SSH fails on this machine, please 
install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to 
work properly,
    default: in which case you may ignore this message.
```

A připojíme se k běžícímu serveru

```
vagrant ssh
Linux debian 4.9.0-4-amd64 #1 SMP Debian 4.9.51-1 (2017-09-28) x86_64

The programs included with the Debian GNU/Linux system are free 
software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Nov 21 15:14:33 2017
vagrant@debian:~$
```

Pokud se připojení zdařilo a vidíte podobný výpis, máte vyhráno.


