---
title: "Zakladni setup pracovni stanice pro Nette"
date: 2018-06-30 23:54:32+01:00
featured_image: '/images/hero.png'
tags: ["nette"]
draft: true
---

# Add user to usergroup
```
usermod -a -G $USER http
```
# ssh key

```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

```
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

```
sudo apt-get install xcli
xclip -sel clip < ~/.ssh/id_rsa.pub
```

# Setup web server (Apache) 
## Prepare directory

```
sudo chmod g+w ./http/
sudo chown root:http ./http/
```
## Instalovat httpd, php, mysql
Nainstalujeme potřebné balíčky

#### Arch/Antergos
```
pacman -S php php-cli php-apache percona-server percona-server-clients composer adminer curl git unzip
```

#### Debian/Ubuntu
```
apt-get install apache2 mysql-server php php-mysql libapache2-mod-php php-mysql curl git unzip net-tools composer -y
```

### Konfigurace - debian/ubuntu
```
sudo nano /etc/apache2/mods-enabled/dir.conf

Presunout index.php na prvni misto

<IfModule mod_dir.c>
    DirectoryIndex index.php index.html ....
</IfModule>
```

### Konfigurace - Arch/Antergos

```
sudo nano /etc/httpd/conf/httpd.conf 
```

**zakomentujeme**
```
LoadModule mpm_event_module modules/mod_mpm_event.so
```

**odkomentujeme**
```
LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
```

Přidáme na konec souboru
```
LoadModule php7_module modules/libphp7.so
AddHandler php7-script php
Include conf/extra/php7_module.conf
```

### Restartujeme sluzbu
```
systemctl restart httpd
```

### Test funkcnosti

#### Debian/Ubuntu
```
nano /var/www/html/info.php
```
#### Arch/Antergos
```
nano /srv/http/info.php
```
```
<?php
infophp();
?>
```

# Enable iconv in php (required by nette framework)

```
sudo nano /etc/php/php.ini 
```

uncomment
extension=iconv

# Run composer to setup project directory
```
composer create-project nette/web-project nette-blog
```

Make directories ./log and ./temp writable
```
sudo chmod 775 log
sudo chmod 775 temp
```


# Mysql

Vytvoreni noveho uzivatele

```
sudo -i
mysql
CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypassword'
```

```
sudo nano /etc/php/php.ini 
```
```
sudo nano /etc/php/7.2/apache2/php.ini
```


uncomment

```
extension=pdo_sqlite.so
```

## Install adminer
```
sudo apt-get install adminer
```


```
echo "Alias /adminer.php /usr/share/adminer/adminer/index.php" | sudo tee /etc/apache2/conf-avaliable/adminer.conf
sudo a2enconf adminer.conf
sudo systemctl restart apache2
```

http://localhost.local/adminer.php

### Ruzne

sudo su -c "mysqld --skip-grant-tables &" -s /bin/sh mysql

Let's tell the database server to reload the grant tables by issuing the FLUSH PRIVILEGES command.

FLUSH PRIVILEGES;
Now we can actually change the root password.

For MySQL 5.7.6 and newer as well as MariaDB 10.1.20 and newer, use the following command.

ALTER USER 'root'@'localhost' IDENTIFIED BY 'new_password';
For MySQL 5.7.5 and older as well as MariaDB 10.1.20 and older, use:

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('new_password');
Make sure to replace new_password with your new password of choice.





